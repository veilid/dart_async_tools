import 'package:async_tools/async_tools.dart';
import 'package:test/test.dart';

void main() {
  group('AsyncValue tests', () {
    test('Simple data test', () async {
      expect(const AsyncValue.data('3'), isA<AsyncData<String>>());
      expect(const AsyncValue<String>.loading(), isA<AsyncLoading<String>>());
      expect(AsyncValue<String>.error(Exception('test')),
          isA<AsyncError<String>>());
    });
  });
}
