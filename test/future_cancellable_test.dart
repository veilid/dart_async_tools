import 'dart:async';

import 'package:async_tools/async_tools.dart';
import 'package:test/test.dart';

void main() {
  group('CancelValueRequest tests', () {
    test('Void no cancel', () async {
      final fut = Future<void>.delayed(const Duration(seconds: 1));

      final cancelValueRequest = CancelValueRequest<void>();

      await expectLater(
          () async => fut.withCancelValue(cancelValueRequest), isA<void>());
    });

    test('Void with cancel', () async {
      final fut = Future<void>.delayed(const Duration(seconds: 1));

      final cancelValueRequest = CancelValueRequest<void>()..cancel();

      await expectLater(() async => fut.withCancelValue(cancelValueRequest),
          throwsA(isA<CancelException>()));
    });

    test('Void with cancelValue', () async {
      final fut =
          Future<String>.delayed(const Duration(seconds: 1), () => 'normal');

      final cancelValueRequest = CancelValueRequest<String>()
        ..cancelValue('cancelled');

      expect(
          await fut.withCancelValue(cancelValueRequest), equals('cancelled'));
    });

    test('Void with cancelError', () async {
      final fut =
          Future<String>.delayed(const Duration(seconds: 1), () => 'normal');

      final cancelRequest = CancelValueRequest<String>()
        ..cancelError(TimeoutException(null));

      await expectLater(() async => fut.withCancelValue(cancelRequest),
          throwsA(isA<TimeoutException>()));
    });
  });

  group('CancelRequest tests', () {
    test('no cancel', () async {
      final fut = Future<void>.delayed(const Duration(seconds: 1));

      final cancelRequest = CancelRequest();

      await expectLater(() async => fut.withCancel(cancelRequest), isA<void>());
    });

    test('with cancel', () async {
      final fut = Future<void>.delayed(const Duration(seconds: 1));

      final cancelRequest = CancelRequest()..cancel();

      await expectLater(() async => fut.withCancel(cancelRequest),
          throwsA(isA<CancelException>()));
    });

    test('with cancel twice', () async {
      final fut = Future<void>.delayed(const Duration(seconds: 1));

      final cancelRequest = CancelRequest();

      expect(cancelRequest.cancel(), equals(true));
      expect(cancelRequest.cancel(), equals(false));

      await expectLater(() async => fut.withCancel(cancelRequest),
          throwsA(isA<CancelException>()));
    });

    test('with cancel after', () async {
      final fut = Future<void>.delayed(const Duration(seconds: 1));

      final cancelRequest = CancelRequest();

      await fut.withCancel(cancelRequest);

      cancelRequest.cancel();
    });

    test('with cancel during', () async {
      final fut = Future<void>.delayed(const Duration(seconds: 1));

      final cancelRequest = CancelRequest();

      final cancelFut =
          Future<void>.delayed(const Duration(milliseconds: 500), () async {
        cancelRequest.cancel();
      });

      await expectLater(() async => fut.withCancel(cancelRequest),
          throwsA(isA<CancelException>()));

      await cancelFut;
    });

    test('Void with cancelError', () async {
      final fut =
          Future<String>.delayed(const Duration(seconds: 1), () => 'normal');

      final cancelRequest = CancelRequest()
        ..cancelError(TimeoutException(null));

      await expectLater(() async => fut.withCancel(cancelRequest),
          throwsA(isA<TimeoutException>()));
    });
  });
}
