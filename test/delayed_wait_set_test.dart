import 'package:async_tools/async_tools.dart';
import 'package:test/test.dart';

void main() {
  group('DelayedWaitSet default tests', () {
    test('empty test', () async {
      final dws = DelayedWaitSet<void, void>();
      final res = await dws();
      expect(res, isEmpty);
    });

    test('single result', () async {
      final dws = DelayedWaitSet<String, void>()..add((_) async => 'res1');
      final res = await dws();
      expect(res, hasLength(equals(1)));
      expect(res[0], equals('res1'));
    });

    test('multiple results', () async {
      final dws = DelayedWaitSet<String, void>()
        ..add((_) async => 'res1')
        ..add((_) async => 'res2')
        ..add((_) async => 'res3')
        ..add((_) async => 'res4');
      final res = await dws();
      expect(res, hasLength(equals(4)));
      expect(res[0], equals('res1'));
      expect(res[1], equals('res2'));
      expect(res[2], equals('res3'));
      expect(res[3], equals('res4'));
    });

    test('multiple results with delays', () async {
      final dws = DelayedWaitSet<String, void>()
        ..add((_) => Future<String>.delayed(
            const Duration(seconds: 2), () async => 'res1'))
        ..add((_) async => 'res2')
        ..add((_) => Future<String>.delayed(
            const Duration(seconds: 1), () async => 'res3'))
        ..add((_) async => 'res4');
      final res = await dws();
      expect(res, hasLength(equals(4)));
      expect(res[0], equals('res1'));
      expect(res[1], equals('res2'));
      expect(res[2], equals('res3'));
      expect(res[3], equals('res4'));
    });

    test('multiple results twice', () async {
      final dws = DelayedWaitSet<String, void>()
        ..add((_) async => 'res1')
        ..add((_) async => 'res2')
        ..add((_) async => 'res3')
        ..add((_) async => 'res4');
      final res = await dws();

      dws
        ..add((_) async => 'res5')
        ..add((_) async => 'res6')
        ..add((_) async => 'res7')
        ..add((_) async => 'res8');

      final res2 = await dws();

      expect(res, hasLength(equals(4)));
      expect(res[0], equals('res1'));
      expect(res[1], equals('res2'));
      expect(res[2], equals('res3'));
      expect(res[3], equals('res4'));

      expect(res2, hasLength(equals(4)));
      expect(res2[0], equals('res5'));
      expect(res2[1], equals('res6'));
      expect(res2[2], equals('res7'));
      expect(res2[3], equals('res8'));
    });

    test('multiple results twice with delayed await', () async {
      final dws = DelayedWaitSet<String, void>()
        ..add((_) async => 'res1')
        ..add((_) async => 'res2')
        ..add((_) async => 'res3')
        ..add((_) async => 'res4');
      final resFut = dws();

      dws
        ..reset()
        ..add((_) async => 'res5')
        ..add((_) async => 'res6')
        ..add((_) async => 'res7')
        ..add((_) async => 'res8');

      final res2Fut = dws();

      final res = await resFut;
      final res2 = await res2Fut;

      expect(res, hasLength(equals(4)));
      expect(res[0], equals('res1'));
      expect(res[1], equals('res2'));
      expect(res[2], equals('res3'));
      expect(res[3], equals('res4'));

      expect(res2, hasLength(equals(4)));
      expect(res2[0], equals('res5'));
      expect(res2[1], equals('res6'));
      expect(res2[2], equals('res7'));
      expect(res2[3], equals('res8'));

      final res3 = await dws();
      expect(res3, isEmpty);
    });
  });

  group('DelayedWaitSet chunked tests', () {
    test('empty test', () async {
      final dws = DelayedWaitSet<void, void>();
      final res = await dws(
          chunkSize: 4,
          onChunkDone: (x) {
            throw StateError('unreachable');
          });
      expect(res, isEmpty);
    });

    test('single result', () async {
      final dws = DelayedWaitSet<String, void>()..add((_) async => 'res1');
      var totalChunks = 0;
      var totalChunkElements = 0;
      final res = await dws(
          chunkSize: 4,
          onChunkDone: (x) {
            totalChunks++;
            totalChunkElements += x.length;
            return true;
          });
      expect(totalChunks, equals(1));
      expect(totalChunkElements, equals(1));
      expect(res, hasLength(equals(1)));
      expect(res[0], equals('res1'));
    });

    test('multiple results', () async {
      final dws = DelayedWaitSet<String, void>()
        ..add((_) async => 'res1')
        ..add((_) async => 'res2')
        ..add((_) async => 'res3')
        ..add((_) async => 'res4');

      var totalChunks = 0;
      var totalChunkElements = 0;
      final res = await dws(
          chunkSize: 2,
          onChunkDone: (x) {
            totalChunks++;
            totalChunkElements += x.length;
            return true;
          });

      expect(totalChunks, equals(2));
      expect(totalChunkElements, equals(4));

      expect(res, hasLength(equals(4)));
      expect(res[0], equals('res1'));
      expect(res[1], equals('res2'));
      expect(res[2], equals('res3'));
      expect(res[3], equals('res4'));
    });

    test('multiple results odd', () async {
      final dws = DelayedWaitSet<String, void>()
        ..add((_) async => 'res1')
        ..add((_) async => 'res2')
        ..add((_) async => 'res3')
        ..add((_) async => 'res4')
        ..add((_) async => 'res5');

      var totalChunks = 0;
      var totalChunkElements = 0;
      final res = await dws(
          chunkSize: 2,
          onChunkDone: (x) {
            totalChunks++;
            totalChunkElements += x.length;
            return true;
          });

      expect(totalChunks, equals(3));
      expect(totalChunkElements, equals(5));

      expect(res, hasLength(equals(5)));
      expect(res[0], equals('res1'));
      expect(res[1], equals('res2'));
      expect(res[2], equals('res3'));
      expect(res[3], equals('res4'));
      expect(res[4], equals('res5'));

      final res2 = await dws();
      expect(res2, isEmpty);
    });

    test('multiple results stop early', () async {
      final dws = DelayedWaitSet<String, void>()
        ..add((_) async => 'res1')
        ..add((_) async => 'res2')
        ..add((_) async => 'res3')
        ..add((_) async => 'res4')
        ..add((_) async => 'res5');

      var totalChunks = 0;
      var totalChunkElements = 0;
      final res = await dws(
          chunkSize: 2,
          onChunkDone: (x) {
            totalChunks++;
            totalChunkElements += x.length;
            if (totalChunks == 2) {
              return false;
            }
            return true;
          });

      expect(totalChunks, equals(2));
      expect(totalChunkElements, equals(4));

      expect(res, hasLength(equals(4)));
      expect(res[0], equals('res1'));
      expect(res[1], equals('res2'));
      expect(res[2], equals('res3'));
      expect(res[3], equals('res4'));

      final res2 = await dws();
      expect(res2, isEmpty);
    });

    test('cancels', () async {
      final dws = DelayedWaitSet<String, bool>()
        ..add((c) async {
          while (!c.isCompleted) {
            await Future.delayed(
                const Duration(milliseconds: 250), () async {});
          }
          return 'cancelled loop';
        })
        ..add((c) async {
          await c.future;
          return 'cancelled future';
        })
        ..add((_) async => 'finished normally');

      final res = await dws(cancelValue: true);

      expect(res, hasLength(equals(3)));
      expect(res[0], equals('cancelled loop'));
      expect(res[1], equals('cancelled future'));
      expect(res[2], equals('finished normally'));

      final res3 = await dws();
      expect(res3, isEmpty);
    });
  });
}
