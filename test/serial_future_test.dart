import 'package:async_tools/async_tools.dart';
import 'package:collection/collection.dart';
import 'package:test/test.dart';

void main() {
  group('SerialFuture default tests', () {
    test('done test', () async {
      const sf1 = 'serialfuture 1';
      final res = <String>[];
      serialFuture(sf1, () async {
        res.add('fut');
        return 'done';
      }, onDone: res.add);
      await serialFuturePause(sf1);

      expect(res, equals(['fut', 'done']));
    });

    test('ordering test', () async {
      const sf2 = 'serialfuture 2';
      final res = <String>[];
      serialFuture(sf2, () async {
        res.add('fut1');
        return 'done1';
      }, onDone: res.add);
      serialFuture(sf2, () async {
        res.add('fut2');
        return 'done2';
      }, onDone: res.add);
      serialFuture(sf2, () async {
        res.add('fut3');
        return 'done3';
      }, onDone: res.add);
      serialFuture(sf2, () async {
        res.add('fut4');
        return 'done4';
      }, onDone: res.add);
      await serialFuturePause(sf2);

      expect(
          res,
          equals([
            'fut1',
            'done1',
            'fut2',
            'done2',
            'fut3',
            'done3',
            'fut4',
            'done4'
          ]));
    });

    test('pause resume test', () async {
      const sf3 = 'serialfuture 3';
      final res = <String>[];
      serialFuture(sf3, () async {
        res.add('fut1');
        return 'done1';
      }, onDone: res.add);

      await serialFuturePause(sf3);
      serialFutureResume(sf3);

      serialFuture(sf3, () async {
        res.add('fut2');
        return 'done2';
      }, onDone: res.add);

      await serialFuturePause(sf3);

      serialFuture(sf3, () async {
        res.add('fut3');
        return 'done3';
      }, onDone: res.add);
      serialFuture(sf3, () async {
        res.add('fut4');
        return 'done4';
      }, onDone: res.add);

      serialFutureResume(sf3);
      await serialFuturePause(sf3);

      expect(
          res,
          equals([
            'fut1',
            'done1',
            'fut2',
            'done2',
            'fut3',
            'done3',
            'fut4',
            'done4'
          ]));
    });

    test('parallel test', () async {
      const sf4 = 'serialfuture 4';
      final res = <int>{};
      final done = <int>{};

      await Iterable<int>.generate(1000)
          .toList()
          .slices(10)
          .map((s) => Future.delayed(Duration.zero, () async {
                for (final x in s) {
                  serialFuture(sf4, () async {
                    res.add(x);
                    return x;
                  }, onDone: done.add);
                }
              }))
          .wait;

      await serialFuturePause(sf4);

      final out = Iterable<int>.generate(1000).toSet();

      expect(res, equals(out));
      expect(done, equals(out));
    });

    test('serial test', () async {
      const sf5 = 'serialfuture 5';

      const parallel = 10;

      final res = List<List<int>>.filled(parallel, []);
      final done = List<List<int>>.filled(parallel, []);

      await Iterable<int>.generate(parallel)
          .map((s) => Future.delayed(Duration.zero, () async {
                for (var x = 0; x < 1000; x++) {
                  serialFuture(sf5, () async {
                    res[s].add(x);
                    return x;
                  }, onDone: done[s].add);
                }
              }))
          .wait;

      await serialFuturePause(sf5);

      final out = Iterable<int>.generate(1000).toSet();

      for (var s = 0; s < parallel; s++) {
        expect(res[s], equals(out));
        expect(done[s], equals(out));
      }
    });
  });
}
