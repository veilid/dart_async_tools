import 'package:async_tools/async_tools.dart';

Future<void> main() async {
  final waitSet = WaitSet<void, void>();
  for (var n = 0; n < 10; n++) {
    waitSet.add((_) async => Future.delayed(Duration(seconds: n), () {
          // ignore: avoid_print
          print('$n');
        }));
  }
  await waitSet();
}
