# Collection of useful tools for async/Future code in Dart

Tests are not complete

Mutex is extended and borrow from Mutex package

## Contains

* AsyncTagLock - Lock access to a resource based on an object instance
* AsyncValue - Like riverpod's AsyncValue, but without Riverpod. Can represent 'data', 'loading', and 'error' states for a value generated asynchronously.
* WaitSet - Execute a set of async closures and wait for them to all to finish
* DelayedWaitSet - Execute a set of async closures starting at the same time, and wait for them all to finish
* serialFuture - Process a single future at a time per tag queued serially
* singleFuture - Process a single future at a time per tag
* SingleStateProcessor - Process a single state update at a time ensuring the most recent state gets processed asynchronously, possibly skipping states that happen while a previous state is still being processed
* SingleStatelessProcessor - Process a single stateless update at a time ensuring each request gets processed asynchronously, and continuously while update is requested

