## 0.1.7
- Fix missing subscription cancel

## 0.1.6
- Clarify SingleStateProcessor code
- Add mutex and rwlock deadlock debugging capability

## 0.1.5
- Add FutureCancellable
- Add SingleStateProcessor lifecycle callback
- lifecycle bugfixes

## 0.1.4
- Add support for cancellation in WaitSet and DelayedWaitSet
- Add simple 'asyncSleep' function

## 0.1.3
- Clean up bugs in SingleStateProcessor
- Add SingleStateProcessorMap

## 0.1.2
- Add pause/resume for serialFuture and add tests

## 0.1.1
- Add return values for WaitSet and DelayedWaitSet

## 0.1.0
- Initial version
