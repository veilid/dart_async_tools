import 'dart:async';
import 'dart:collection';

import 'async_tag_lock.dart';

AsyncTagLock<Object> _keys = AsyncTagLock();
typedef SerialFutureQueueItem = Future<void> Function();
Map<Object, Queue<SerialFutureQueueItem>> _queues = {};

SerialFutureQueueItem _makeSerialFutureQueueItem<T>(
        Future<T> Function() closure,
        void Function(T)? onDone,
        void Function(Object e, StackTrace? st)? onError) =>
    () async {
      try {
        final out = await closure();
        if (onDone != null) {
          onDone(out);
        }
        // ignore: avoid_catches_without_on_clauses
      } catch (e, sp) {
        if (onError != null) {
          onError(e, sp);
        } else {
          rethrow;
        }
      }
    };

/// Process a single future at a time per tag queued serially
///
/// The closure function is called to produce the future that is to be executed.
/// If a future with a particular tag is still executing, it is queued serially
/// and executed when the previous tagged future completes.
/// When a tagged serialFuture finishes executing, the onDone callback is called.
/// If an unhandled exception happens in the closure future, the onError callback
/// is called.
void serialFuture<T>(Object tag, Future<T> Function() closure,
    {void Function(T)? onDone,
    void Function(Object e, StackTrace? st)? onError}) {
  final queueItem = _makeSerialFutureQueueItem(closure, onDone, onError);
  if (!_keys.tryLock(tag)) {
    // Queue is either running or we are paused
    _queues.putIfAbsent(tag, Queue.new).add(queueItem);
    return;
  }
  assert(!_queues.containsKey(tag), 'queue should not exist yet');
  final queue = _queues[tag] = Queue.from([queueItem]);
  _runQueue(tag, queue);
}

void _runQueue(Object tag, Queue<Future<void> Function()> queue) {
  unawaited(() async {
    assert(_keys.isLocked(tag), 'should be locked here');
    do {
      final queueItem = queue.removeFirst();
      await queueItem();
    } while (queue.isNotEmpty);
    _queues.remove(tag);
    _keys.unlockTag(tag);
  }());
}

Future<void> serialFuturePause(Object tag) async {
  await _keys.lockTag(tag);
  // If we get this lock there is no queue
}

void serialFutureResume(Object tag) {
  final queue = _queues[tag];
  if (queue != null) {
    // Things were added while paused
    _runQueue(tag, queue);
  } else {
    // No queue to run, unlock
    _keys.unlockTag(tag);
  }
}

Future<void> serialFutureClose(Object tag) async {
  await _keys.lockTag(tag);
  // If we get this lock there is no queue
  _keys.unlockTag(tag);
}
