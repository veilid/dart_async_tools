import '../async_tools.dart';

/// A map-like data structure that manages continuous single-state updates
/// for a set of keys.
/// * Can perform individual state updates via [updateState]
/// * Can handle a stream of updates via [follow]
class SingleStateProcessorMap<Key, State> {
  SingleStateProcessorMap();

  /// Shut down this [SingleStateProcessorMap]
  /// This object is unusable after calling this function
  Future<void> close() async {
    await _processorMap.values.map((v) => v.close()).wait;
  }

  ////////////////////////////////////////////////////////////////////////////
  // Public Interface

  Future<void> remove(Key key) async =>
      await _processorMap.remove(key)?.close();

  /// Subscribe to a stream of changes and update the state for a specific key
  void follow(Key key, Stream<State> stream, State? initialState,
          Future<void> Function(State) closure) =>
      _processorMap
          .putIfAbsent(key, SingleStateProcessor<State>.new)
          .follow(stream, initialState, closure);

  /// Unsubscribe from the current stream for a specific key
  Future<void> unfollow(Key key) async => await _processorMap[key]?.unfollow();

  /// Update the single-state processor for a specific key
  /// Returns true if the state change was processed immediately
  /// Returns false if the state change was enqueued for later processing
  bool updateState(
          Key key, State newInputState, Future<void> Function(State) closure) =>
      _processorMap
          .putIfAbsent(key, SingleStateProcessor<State>.new)
          .updateState(newInputState, closure);

  /// Pause single-state processing for a specific key
  Future<void> pause(Key key) async => await _processorMap[key]?.pause();

  /// Resume single-state processing for a specific key
  Future<void> resume(Key key) async => await _processorMap[key]?.resume();

  ////////////////////////////////////////////////////////////////////////////
  // Fields

  final _processorMap = <Key, SingleStateProcessor<State>>{};
}
