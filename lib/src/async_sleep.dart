/// Sleep asynchronously for a specified duration, or 1 second if not specified
Future<void> asyncSleep([Duration? duration]) async =>
    Future.delayed(duration ?? const Duration(seconds: 1), () {});
