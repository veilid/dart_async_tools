import 'dart:async';

import '../async_tools.dart';

/// Process a single state update at a time ensuring the most
/// recent state gets processed asynchronously, possibly skipping
/// states that happen while a previous state is still being processed.
///
/// Eventually this will always process the most recent state passed to
/// updateState.
///
/// This is useful for processing state changes asynchronously without waiting
/// from a synchronous execution context.
///
/// 'onStateProcessing' callback tracks which state is bering processed
/// or null when all queued states are done processing.
///
/// Optionally follows state changes from a stream.
class SingleStateProcessor<State> {
  SingleStateProcessor({Future<void> Function(State?)? onStateProcessing})
      : _onStateProcessing = onStateProcessing;

  void follow(Stream<State> stream, State? initialState,
      Future<void> Function(State) closure) {
    _streamSubscription = stream.listen((data) {
      updateState(data, closure);
    }, onDone: () {
      _streamSubscription = null;
    });
    if (initialState != null) {
      updateState(initialState, closure);
    }
  }

  Future<void> unfollow() async {
    final sub = _streamSubscription;
    if (sub != null) {
      _streamSubscription = null;
      await sub.cancel();
    }
  }

  Future<void> close() async {
    // Accept no more follow states
    await _streamSubscription?.cancel();
    // Wait for the singleFuture to exit
    await pause();
    // Mark as closed
    _closed = true;
    // Release the lock on it without running any more closures
    singleFutureResume(this);
  }

  /// Change the desired state and either run a closure or enqueue it for
  /// running later.
  ///
  /// Returns true if the state change was processed immediately.
  ///
  /// Returns false if the state change was enqueued for later processing
  /// or dropped if closed.
  bool updateState(State newInputState, Future<void> Function(State) closure) {
    if (_closed) {
      return false;
    }

    var processed = true;

    // Use a singlefuture here to ensure we get dont lose any updates
    // If the input stream gives us an update while we are
    // still processing the last update, the most recent input state will
    // be saved and processed eventually.
    singleFuture(this, () async {
      var newState = newInputState;
      var newClosure = closure;
      var done = false;
      while (!done) {
        await _onStateProcessing?.call(newState);

        await newClosure(newState);

        // See if there's another state change to process
        final nextState = _nextState;
        final nextClosure = _nextClosure;
        _nextState = null;
        _nextClosure = null;
        if (nextState != null) {
          newState = nextState;
          newClosure = nextClosure!;
        } else {
          done = true;
        }
      }
      await _onStateProcessing?.call(null);
    }, onBusy: () {
      // Keep this state until we process again
      _nextState = newInputState;
      _nextClosure = closure;
      processed = false;
    });

    return processed;
  }

  Future<void> pause() => singleFuturePause(this);
  Future<void> resume() async {
    // Process any next state before resuming the singlefuture
    try {
      final nextState = _nextState;
      final nextClosure = _nextClosure;
      _nextState = null;
      _nextClosure = null;
      if (nextState != null) {
        await nextClosure!(nextState);
      }
    } finally {
      singleFutureResume(this);
    }
  }

  final Future<void> Function(State?)? _onStateProcessing;
  State? _nextState;
  Future<void> Function(State)? _nextClosure;
  StreamSubscription<State>? _streamSubscription;
  bool _closed = false;
}
