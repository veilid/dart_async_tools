import 'dart:async';

import 'package:collection/collection.dart';

/// Execute a set of async closures (functions returninga Future) in batches.
///
/// Closures are not run until this object is called and only when each chunk
/// is being processed, not when they are added like WaitSet. Each chunk
/// should be treated like it is executing in an unordered fashion, however
/// the results will still be returned in the order the closures were added.
///
/// If chunkSize is set to a number greater than zero, processing will
/// be done in groups of that size. If the number of closures to process is
/// not a multiple of the chunkSize, the final chunk will contain fewer
/// elements.
///
/// If onChunkDone is specified, it will be called at the end of processing
/// each batch with the set of values returned from the futures in the order
/// they were added.
///
/// Returns the output of all of the chunks in a list in the same order
/// they were added.
class DelayedWaitSet<T, C> {
  DelayedWaitSet();

  /// Add a closure to be waited on for completion.
  /// The closure will not run until the DelayedWaitSet is waited on.
  /// The closure takes a 'cancelled' completer parameter
  /// that can be used to determine if the closure should exit early.
  void add(Future<T> Function(Completer<C>) closure) {
    _closures.add(closure);
  }

  /// Stop waiting for any futures and start a new wait set
  void reset({C? cancelValue}) {
    if (cancelValue != null && !_cancelled.isCompleted) {
      _cancelled.complete(cancelValue);
    }
    _closures = [];
    _cancelled = Completer<C>();
  }

  /// Run all added closures, optionally in chunked batches,
  /// and wait for all result futures
  Future<List<T>> call(
      {int chunkSize = 0,
      FutureOr<bool> Function(List<T>)? onChunkDone,
      C? cancelValue}) async {
    if (cancelValue != null && !_cancelled.isCompleted) {
      _cancelled.complete(cancelValue);
    }

    // Take closures
    final closures = _closures;

    // Return early if there's nothing to do
    if (closures.isEmpty) {
      _cancelled = Completer<C>();
      return [];
    }

    // Make chunks
    final closuresChunks =
        (chunkSize > 0) ? closures.slices(chunkSize) : [closures];
    final onChunkDoneFunc = onChunkDone;

    // Process chunks
    final out = <T>[];
    for (final chunk in closuresChunks) {
      final chunkOut = await chunk.map((c) => c(_cancelled)).wait;
      out.addAll(chunkOut);
      if (onChunkDoneFunc != null) {
        final more = await onChunkDoneFunc(chunkOut);
        if (!more) {
          break;
        }
      }
    }

    _closures = [];
    _cancelled = Completer<C>();

    return out;
  }

  List<Future<T> Function(Completer<C>)> _closures = [];
  var _cancelled = Completer<C>();
}
