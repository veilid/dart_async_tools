import 'dart:async';

/// Execute a set of async closures (functions returning a Future) immediately
/// but wait until they all finish. Closures are immediately executed upon
/// calling 'add'.
///
/// Awaiting this object will return the set of return values from the Futures
/// once they have all completed.
class WaitSet<T, C> {
  WaitSet();

  /// Add a closure to be waited on for completion
  /// The closure will run immediately and its result of its future will be
  /// available when the WaitSet is waited on.
  /// The closure takes a 'cancelled' completer parameter
  /// that can be used to determine if the closure should exit early.
  void add(Future<T> Function(Completer<C>) closure) {
    _futures
        .add(Future.delayed(Duration.zero, () async => closure(_cancelled)));
  }

  /// Stop waiting for any futures and start a new wait set
  void reset({C? cancelValue}) {
    if (cancelValue != null && !_cancelled.isCompleted) {
      _cancelled.complete(cancelValue);
    }
    _futures = [];
    _cancelled = Completer<C>();
  }

  /// Wait for all added futures
  Future<List<T>> call({C? cancelValue}) async {
    if (cancelValue != null && !_cancelled.isCompleted) {
      _cancelled.complete(cancelValue);
    }

    // Take futures
    final futures = _futures;

    // Wait unless there's nothing to do
    if (futures.isEmpty) {
      _cancelled = Completer<C>();
      return [];
    }

    final res = await futures.wait;

    // Reset the future list so we can do more
    _futures = [];
    _cancelled = Completer<C>();
    return res;
  }

  List<Future<T>> _futures = [];
  var _cancelled = Completer<C>();
}
