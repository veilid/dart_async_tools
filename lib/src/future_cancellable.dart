import 'dart:async';

class CancelException implements Exception {
  const CancelException([this.cause = 'operation was cancelled']);
  final String cause;
}

class CancelValueRequest<T> extends _CancelRequestBase<T> {
  CancelValueRequest({this.defaultValue});

  bool cancelValue([FutureOr<T>? value]) {
    if (_completer.isCompleted) {
      return false;
    }
    _completer.complete(value ?? defaultValue);
    return true;
  }

  final T? defaultValue;
}

class CancelRequest extends _CancelRequestBase<void> {}

class _CancelRequestBase<T> {
  _CancelRequestBase() : _completer = Completer<T>();

  bool cancel([String cause = 'operation was cancelled']) {
    if (_completer.isCompleted) {
      return false;
    }
    _completer.completeError(CancelException(cause));
    return true;
  }

  bool cancelError(Object error, [StackTrace? stackTrace]) {
    if (_completer.isCompleted) {
      return false;
    }
    _completer.completeError(error, stackTrace);
    return true;
  }

  final Completer<T> _completer;
}

extension FutureCancellableExt<T> on Future<T> {
  Future<T> withCancelValue(CancelValueRequest<T> cancelValueRequest) async =>
      Future.any([this, cancelValueRequest._completer.future]);
  Future<T> withCancel(CancelRequest cancelRequest) async => Future.any([
        this,
        () async {
          await cancelRequest._completer.future;
          throw StateError('should have cancelled with an exception');
        }()
      ]);
}
